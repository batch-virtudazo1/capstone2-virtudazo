const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth"); 

module.exports.registerUser = (req,res)=>{
	const hashedPw = bcrypt.hashSync(req.body.password,10);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo

	})

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.loginUser = (req,res)=>{
	User.findOne({email: req.body.email})
	.then(foundUser => {
		if(foundUser === null){
				return res.send({message: "No user found."});
		} else {
			const isPasswordCOrrect = bcrypt.compareSync(req.body.password, foundUser.password);
			if(isPasswordCOrrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)})

				console.log("We will create a token for the user if the password is correct");
			} else{
				return res.send({message: "incorrect Password"})

			}
		}
	})
}
module.exports.getUserDetails = (req,res)=>{
	let userId = req.user.id;
	User.findById(userId)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}


module.exports.checkEmailExists = (req,res) => {

	User.findOne({email: req.body.email})
	.then(result => {

		//console.log(result)

		if(result === null){
			return res.send(false);
		} else {
			return res.send(true)
		}

	})
	.catch(err => res.send(err));
}


module.exports.checkoutOrders = (req,res) =>{

	if(req.user.isAdmin){
		return res.send({message: "Action Forbidden"});
	}
	User.findById(req.user.id).then(user => {				
		let newOrder = {
			totalAmount: req.body.totalAmount,
			products: req.body.products
		}
		user.orders.push(newOrder);

		let initialOrderId = user.orders[user.orders.length-1];
		newOrder.products.forEach(function(product){
			Product.findById(product.productId).then(products => {
				let buyer = {
						orderId: initialOrderId.id,		
						userId: req.user.id,
						quantity: product.quantity
					}
				products.orders.push(buyer);
				return products.save().then(products => true).catch(err => err.message)
			})
		})
		return user.save().then(user => true).catch(err => err.message)
	})
	return res.send(true)
}


module.exports.setUserAsAdmin = (req,res) => {
	let update = {
		isAdmin: true
	}
	User.findByIdAndUpdate(req.params.userId, update, {new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.getUserOrders = (req,res)=>{
	User.findById(req.user.id)
	.then(user => res.send(user.orders))
	.catch(err => err.message)
}

module.exports.getAllUsersOrders = (req,res)=>{
	User.find({isAdmin: false, orders: {$ne:[]}})
	.then(result => res.send(result))
	.catch(err => err.message)
}


// stretch Goal # 1

 module.exports.activateProduct = (req,res) => {
 	let update = {
 		isActive: true
 	}
 	Product.findByIdAndUpdate(req.params.productId, update, {new:true})
 	.then(result => res.send(result))
 	.catch(error => res.send(error))

 }

// Stretch Goal # 2

module.exports.displayProduct = (req,res)=>{
	
	let getOrderId = req.params.orderId;
	let getUserId = req.user.id;
	//console.log(req.user.orders.getOrderId)
	//console.log(req.user)
	console.log(getOrderId)
	User.findById(getUserId)
	.then(result => res.send(result.orders))
	.catch(error => res.send(error))
}