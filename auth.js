const jwt = require("jsonwebtoken");
const secret = "ecommerceAPI";


module.exports.createAccessToken = (userDetails) =>{

	//console.log(userDetails);

	const data = {
		id: userDetails.id,
		email: userDetails.email,
		isAdmin: userDetails.isAdmin
	}
	console.log(data);
	// .sign() will create a JWT using our data object with our secret
	return jwt.sign(data,secret,{});
}


module.exports.verify = (req,res,next) =>{
	let token = req.headers.authorization
	if(typeof token === "undefined"){
		return res.send({auth: "Failed. No token."});
	} else {
		
		// console.log(token);
		token = token.slice(7);
		// console.log(token);


		jwt.verify(token, secret, function(err,decodedToken){

			console.log(decodedToken);
			console.log(err);

			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				})
			} else{
				req.user = decodedToken;

				next();
			}
		})
	}

}
// verify admin

module.exports.verifyAdmin = (req,res,next) => {
		console.log(req.user);

		if(req.user.isAdmin){
			next();
		} else {
			return res.send({

				auth: "failed",
				message: "Action Forbidden"
			})
		}
}
