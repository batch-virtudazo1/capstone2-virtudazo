const express = require("express");
const router = express.Router();

const productControllers = require("../controllers/productControllers");
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

router.get('/', verify, verifyAdmin, productControllers.getAllProducts);

router.get('/getAllActiveProducts', productControllers.getAllActiveProducts);

router.get('/getSingleProduct/:productId', productControllers.getSingleProduct);

router.post('/addProduct', verify, verifyAdmin, productControllers.addProduct);

router.put('/activate/:id',verify, verifyAdmin,productControllers.activate);

router.put('/archiveProduct/:productId', verify, verifyAdmin, productControllers.archiveProduct);

router.put('/updateProduct/:productId', verify, verifyAdmin, productControllers.updateProduct);

router.delete('/deleteProduct/:productId', verify, verifyAdmin, productControllers.deleteProduct);


module.exports = router;