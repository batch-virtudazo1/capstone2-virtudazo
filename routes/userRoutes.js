const express = require("express");
const router = express.Router();

// Import controllers
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

router.post('/', userControllers.registerUser);

router.post('/login', userControllers.loginUser);

router.get('/getUserDetails', verify, userControllers.getUserDetails);

router.post('/checkEmailExists',userControllers.checkEmailExists);

router.post('/checkoutOrders', verify, userControllers.checkoutOrders);

router.put('/setUserAsAdmin/:userId', verify, verifyAdmin, userControllers.setUserAsAdmin);

router.get('/getUserOrders', verify, userControllers.getUserOrders);

router.get('/getAllUsersOrders', verify, verifyAdmin, userControllers.getAllUsersOrders);

// Stretch Goal # 1
router.put('/activateProduct/:productId', verify, verifyAdmin, userControllers.activateProduct);

// Stretch Goal # 2
router.put('/displayProductsPerOrder/:orderId', verify, userControllers.displayProduct);

module.exports = router;