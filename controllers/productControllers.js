const Product = require("../models/Product");
const { ObjectId } = require('mongodb');

module.exports.getAllProducts = (req,res) => {

	Product.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));

}
module.exports.getAllActiveProducts = (req,res)=>{
	Product.find({isActive: true})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.getSingleProduct = (req, res) => {
	Product.findById(req.params.productId)
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.addProduct = (req,res)=>{

	let newProduct = new Product({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	})
	newProduct.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.activate = (req,res) => {

	let updates = {
		isActive: true
	}

	Product.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(err => res.send(err));

}

module.exports.archiveProduct = (req,res) => {
	let update = {
		isActive: false
	}
	Product.findByIdAndUpdate(req.params.productId, update, {new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.updateProduct  = (req, res) => {
	let update = {
		name: req.body.name,
		description: req.body.description,
		price:req.body.price
	}

	Product.findByIdAndUpdate(req.params.productId, update, {new:true})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.deleteProduct = (req,res)  => {

	Product.deleteOne({ _id : new ObjectId(req.params.productId)})
	.then(result => res.send(result))
	.then(error => res.send(error))	
}