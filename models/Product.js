
const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

name: {
	type: String,
	required: [true, "Name of the product is required"]
},
description: {
	type: String,
	required: [true, "Description of the product is required"]
},
price: {
	type: Number,
	required: [true, "Price of the product is required"]
},
isActive: {
	type: Boolean,
	default: true
},
createdOn: {
	type: Date,
	default: new Date()
},  
orders: [
	
	{	
		orderId: {
			type: String,
			required: [true, "Order Id id required"]
		},
		userId: {
			type: String,
			required: [true, "User Id is required"]
		},
		quantity: {
			type: Number,
			required: [true, "Quantity is required"]
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		} 
	}

]

});

module.exports = mongoose.model("Product",productSchema);
